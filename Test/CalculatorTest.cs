﻿using NUnit.Framework;
using System;

namespace fr_stringcalculator.Test
{
    [TestFixture]
    public class CalculatorTest
    {
        [TestCase("1", "2", "3", 6)]
        [TestCase("", "2", "", 2)]
        [TestCase("5", "2", "0", 7)]
        [TestCase("1,2", "0", "0", 1)]
        public void Add_SumThreeNumbers_ReturnSum(string n1, string n2, string n3, int expect)
        {
            var calculator = new CalculatorStrings();

            var res = calculator.Add(n1, n2, n3);

            Assert.AreEqual(expect, res);
        }

        [TestCase(6, "1\n2,3 ")]
        [TestCase(8, "1", "0\n3\n4")]
        [TestCase(5, "1", "1", "1", "1", "1")]
        [TestCase(23, "4", "1\n2\n3", "5", "1", "7")]
        public void Add_UnknownAmountOfNumbers_ReturnSum(int expect, params string[] numbers)
        {
            var calculator = new CalculatorStrings();

            var res = calculator.Add(numbers);

            Assert.AreEqual(expect, res);
        }

        [TestCase(6, "1\n")]
        public void Add_UnknownAmountOfNumbers_ThrowException(int expect, params string[] numbers)
        {
            var calculator = new CalculatorStrings();
            Assert.Throws<Exception>(() => calculator.Add(numbers));
        }

        [TestCase(3, "//;\n1;2")]
        [TestCase(4, "//.\n2.2")]
        [TestCase(7, "\n5\n2")]
        public void Add_WithDifferentDelimiters_ReturnSum(int expect, params string[] numbers)
        {
            var calculator = new CalculatorStringsExtension();

            var res = calculator.Add(numbers);

            Assert.AreEqual(expect, res);
        }

        [TestCase("negatives not allowed", "//;\n-1;2")]
        [TestCase("Vnegatives not allowed: -5, -2", "//;-5;-2")]
        public void Add_NegativeNumbers_ThrowException(string expect, params string[] numbers)
        {
            var calculator = new CalculatorStringsExtension();
            Assert.Throws<Exception>(() => calculator.Add(numbers), expect);
        }
    }
}
