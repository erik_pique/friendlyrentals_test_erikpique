﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace fr_stringcalculator
{
    public class CalculatorStrings
    {
        public int Add(string num1, string num2, string num3)
        {
            double.TryParse(num1, out double number1);
            double.TryParse(num2, out double number2);
            double.TryParse(num3, out double number3);

            return Convert.ToInt32(number1) + Convert.ToInt32(number2) + Convert.ToInt32(number3);
        }

        public int Add(params string[] numbers)
        {
            var pattern = "(\n)";
            int result = 0;
            foreach (var number in numbers)
            {
                if (number.EndsWith("\n"))
                {
                    throw new Exception("Invalid parameters");
                }
                double numberTemp;
                var matches = Regex.Split(Regex.Replace(number, pattern, ","), ",");

                if (matches.Count() > 1)
                {
                    result = result + Add(matches);
                    continue;
                }

                double.TryParse(number, out numberTemp);

                result = result + Convert.ToInt32(numberTemp);
            }

            return result;
        }
    }

    public class CalculatorStringsExtension : CalculatorStrings
    {
        public new int Add(params string[] numbers)
        {
            var negativePattern = @"(-\d)";
            var numbersJoin = string.Join("", numbers);

            if (Regex.IsMatch(numbersJoin, negativePattern))
            {
                var message = "Negatives not allowed";

                var matches = Regex.Matches(numbersJoin, negativePattern);

                if (matches.Count > 1)
                {
                    message = message + ": ";
                    foreach (var matche in matches)
                    {
                        message = string.Join(",", message, matche.ToString());
                    }
                }

                throw new Exception(message);
            }

            var res = Regex.Match(numbersJoin, "^(//.?)");
            if (res.Success)
            {
                var numbersFormater = numbersJoin.Replace(res.Value.Remove(0, 2), ",");
                return base.Add(numbersFormater);
            }
            return base.Add(numbers);
        }
    }
}
